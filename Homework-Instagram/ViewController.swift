//
//  ViewController.swift
//  Homework-Instagram
//
//  Created by Hoeng Linghor on 11/23/20.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var storyView: UICollectionView!
    
    @IBOutlet weak var tbView: UITableView!
    
var data = [
    [
        "name": "lily",
        "profile": "pro2",
        "imgPost": "city",
        "like": "1000 likes",
        "caption": "Have a Nice day!!"
    ],
    [
        "name": "Natalia",
        "profile": "pro3",
        "imgPost": "img5",
        "like": "12000 likes",
        "caption": "Have A Good Holiday ><"
    ],
    [
        "name": "Vanila",
        "profile": "pro4",
        "imgPost": "sea",
        "like": "5300 likes",
        "caption": "Such a good mood :))"
    ],
    [
        "name": "Jack",
        "profile": "pro5",
        "imgPost": "img6",
        "like": "4900 likes",
        "caption": "It's all about Sunday fun day"
    ],
    [
        "name": "Jane",
        "profile": "pro6",
        "imgPost": "view",
        "like": "2200 likes",
        "caption": "Need Vocation!!"
    ],
    [
        "name": "Wayne",
        "profile": "pro7",
        "imgPost": "japan",
        "like": "2800 likes",
        "caption": "Beautiful day in Japan."
    ],
    [
        "name": "Kelly",
        "profile": "pro1",
        "imgPost": "img2",
        "like": "6700 likes",
        "caption": "Love is in the flowers"
    ],

]
    var data1 = [
        ["storyImg":"view",
         "nameStory": "Kelly"
        ],
        ["storyImg":"japan",
         "nameStory": "Wayne"
        ],
        ["storyImg":"sea",
         "nameStory": "Vanila"
        ],
        ["storyImg":"img6",
         "nameStory": "Jack"
        ],
        ["storyImg":"city",
         "nameStory": "lily"
        ],
        ["storyImg":"img5",
         "nameStory": "Natalia"
        ],
        ["storyImg":"img2",
         "nameStory": "Jane"
        ],

    ]
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tbView.delegate = self
        tbView.dataSource = self
        registerCell()
        let logo = UIImage(named: "img-logo")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
    }
   
    func registerCell(){
        storyView.register(UINib(nibName: "StoryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "storyCell")
        tbView.register(UINib(nibName: "PostTableViewCell", bundle: nil), forCellReuseIdentifier: "postCell")
    }
    
}
extension ViewController: UICollectionViewDataSource,UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        data1.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "storyCell", for: indexPath) as! StoryCollectionViewCell
//        cell.storyImg.image = UIImage

        let img = data1[indexPath.row]["storyImg"]!
        cell.storyImg.image = UIImage(named: img)
        cell.nameStory.text = data1[indexPath.row]["nameStory"]
        return cell
    }
    
    
}
extension ViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "postCell", for: indexPath) as! PostTableViewCell
        cell.name.text = data[indexPath.row]["name"]
        cell.like.text = data[indexPath.row]["like"]
        cell.caption.text = data[indexPath.row]["caption"]
        let img = data[indexPath.row]["profile"]!
        print(img)
        cell.profile.image = UIImage(named: img)
        let post = data[indexPath.row]["imgPost"]!
        cell.imgPost.image = UIImage(named: post)
        //        cell.imgPost.image = UIImage(named: obj.imgPost[indexPath.row])
        //        cell.profile.image = UIImage(named: obj.imgPost[indexPath.row])

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        530
    }
}
