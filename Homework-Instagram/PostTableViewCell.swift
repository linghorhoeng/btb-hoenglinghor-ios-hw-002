//
//  PostTableViewCell.swift
//  Homework-Instagram
//
//  Created by Hoeng Linghor on 11/23/20.
//

import UIKit

class PostTableViewCell: UITableViewCell {

    @IBOutlet weak var comment: UITextField!
    @IBOutlet weak var profileComment: UIImageView!
    @IBOutlet weak var caption: UILabel!
    @IBOutlet weak var like: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var profile: UIImageView!
    @IBOutlet weak var imgPost: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
