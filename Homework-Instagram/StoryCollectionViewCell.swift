//
//  StoryCollectionViewCell.swift
//  Homework-Instagram
//
//  Created by Hoeng Linghor on 11/23/20.
//

import UIKit

class StoryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var nameStory: UILabel!
    @IBOutlet weak var storyImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
